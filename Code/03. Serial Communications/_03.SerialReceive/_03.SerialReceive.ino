const int ledPin = 13; // pin the LED is connected to
int   blinkRate = 100;     // blink rate stored in this variable

void setup()
{
  Serial.begin(9600); // Initialize serial port to send and receive at 9600 baud
  pinMode(ledPin, OUTPUT); // set this pin as output
}

/*
void loop()
{
  if ( Serial.available()) // Check to see if at least one character is available
  {
    char ch = Serial.read();
    if(ch >= '0' && ch <= '9') // is this an ascii digit between 0 and 9?
    {
       blinkRate = (ch - '0');      // ASCII value converted to numeric value
       blinkRate = blinkRate * 100; // actual blinkrate is 100 mS times received digit
    }
  }
  blink();
}
*/
/*
int   value;
void loop()
{
  if ( Serial.available() ) 
  {
    char ch = Serial.read();
    if(ch >= '0' && ch <= '9')
    {
       value = (value * 10) + (ch - '0');      
    }
    else if (ch == 10)
    {
       blinkRate = value;
       Serial.println(blinkRate);
       value = 0;
    }
  }
  blink();
}
*/


const int MaxChars = 5; 
char strValue[MaxChars+1];
int index = 0;

void loop()
{
  if ( Serial.available() ) 
  {
     char ch = Serial.read();
     if(index < MaxChars && ch >= '0' && ch <= '9')
        strValue[index++] = ch; // add the ASCII character to the string;
     else
     {
        strValue[index] = 0; 
        blinkRate = atoi(strValue); 
        Serial.println(blinkRate);
        index = 0;
     }
  }
  blink();
}


// blink the LED with the on and off times determined by blinkRate
void blink()
{
  digitalWrite(ledPin,HIGH);
  delay(blinkRate); // delay depends on blinkrate value
  digitalWrite(ledPin,LOW);
  delay(blinkRate);
}
