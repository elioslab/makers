// nunchuck_lines

#include <Wire.h> 
#include "nunchuck.h"

byte accx;

void setup()
{
  Serial.begin(9600);
  nunchuck_setpowerpins();
  nunchuck_init();
}

void loop()
{
  nunchuck_get_data();
  accx  = nunchuck_accelx();
  if( accx >= 75 && accx <= 185)
  {
    // map returns a value from 0 to 63 for values from 75 to 185
    byte  y = map(accx, 75, 185, 0, 63);
    Serial.write(y);
  }
  delay(100); 
}
      
    

