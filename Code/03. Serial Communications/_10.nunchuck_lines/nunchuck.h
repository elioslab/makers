#include <Arduino.h>

#define pwrpin PORTC3
#define gndpin PORTC2

static uint8_t nunchuck_buf[6];

static void nunchuck_setpowerpins() {
    DDRC |= _BV(pwrpin) | _BV(gndpin);
    PORTC &=~ _BV(gndpin);
    PORTC |=  _BV(pwrpin);
    delay(100);          
}

static void nunchuck_init() { 
    Wire.begin();   
    Wire.beginTransmission(0x52);
    Wire.write((uint8_t)0x40);
    Wire.write((uint8_t)0x00);
    delay(100);
    Wire.endTransmission();// stop transmitting
 }

static void nunchuck_send_request() {
    Wire.beginTransmission(0x52);
    Wire.write((uint8_t)0x00);
    Wire.endTransmission();// stop transmitting
}

static char nunchuk_decode_byte (char x) {
    x = (x ^ 0x17) + 0x17;
    return x;
}

static int nunchuck_get_data() {
    int cnt=0;
    Wire.requestFrom (0x52, 6);
    while (Wire.available ()) {
      nunchuck_buf[cnt] = nunchuk_decode_byte( Wire.read() );
      cnt++;
    }
    nunchuck_send_request();
    if (cnt >= 5) {
        return 1; 
    }
    return 0;
}

static int nunchuck_zbutton() { return ((nunchuck_buf[5] >> 0) & 1) ? 0 : 1; }
static int nunchuck_cbutton() { return ((nunchuck_buf[5] >> 1) & 1) ? 0 : 1;  }
static int nunchuck_joyx() { return nunchuck_buf[0]; }
static int nunchuck_joyy() { return nunchuck_buf[1]; }
static int nunchuck_accelx() { return nunchuck_buf[2]; }
static int nunchuck_accely() { return nunchuck_buf[3]; }
static int nunchuck_accelz() { return nunchuck_buf[4]; }
