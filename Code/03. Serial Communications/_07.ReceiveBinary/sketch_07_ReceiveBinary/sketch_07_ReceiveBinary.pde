import processing.serial.*;

Serial myPort; 
short portIndex = 1;
public static final char HEADER = '|';
public static final char MOUSE  = 'M';

void setup()
{
  size(200, 400);
  println(" Connecting to -> " + Serial.list()[portIndex]);
  myPort = new Serial(this,Serial.list()[portIndex], 9600);
}

void draw(){ }

void serialEvent(Serial p) {
  String inString = myPort.readStringUntil('\n');
  if(inString != null) {
      println( inString );   
  }
}

void mousePressed() {
  int index = mouseX;
  int value = mouseY;
  sendMessage(MOUSE, index, value);
}

void sendMessage(char tag, int index, int value) {
  myPort.write(HEADER);
  myPort.write(tag);
  myPort.write(index);
  char c = (char)(value / 256); // msb
  myPort.write(c);
  c = (char)(value & 0xff); // lsb
  myPort.write(c);
}