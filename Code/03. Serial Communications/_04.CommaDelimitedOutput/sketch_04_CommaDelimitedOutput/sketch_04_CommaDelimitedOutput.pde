import processing.serial.*;

Serial myPort;        // Create object from Serial class
char HEADER = 'H';    
short LF = 10;        
short portIndex = 1;  

void setup() {
  size(200, 200);
  println(" Connecting to -> " + Serial.list()[portIndex]);
  myPort = new Serial(this,Serial.list()[portIndex], 9600);
}

void draw() {}

void serialEvent(Serial p)
{
  String message = myPort.readStringUntil(LF);
  if(message != null) {
    print(message);
    String [] data  = message.split(","); 
    if(data[0].charAt(0) == HEADER) {
      for( int i = 1; i < data.length-1; i++) {
        int value = Integer.parseInt(data[i]);
        println("Value" +  i + " = " + value);
      }
      println();
   }
  }
}