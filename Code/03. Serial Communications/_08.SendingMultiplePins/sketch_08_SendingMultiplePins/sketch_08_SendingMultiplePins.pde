import processing.serial.*;

Serial myPort;   
short portIndex = 1;
char HEADER = 'H';

void setup() 
{ 
  size(200, 200);
  println(" Connecting to -> " + Serial.list()[portIndex]);
  myPort = new Serial(this,Serial.list()[portIndex], 9600); 
}

void draw() 
{
  int val;
  if ( myPort.available() >= 15) {  // wait for the entire message to arrive
    if( myPort.read() == HEADER) {  // is this the header
      println("Message received:");
      val = readArduinoInt();
      for(int pin=2, bit=1; pin <= 13; pin++){
        print("digital pin " + pin + " = " );
        int isSet = (val & bit);
        if( isSet == 0) println("0");
        else println("1");
        bit = bit * 2; // shift the bit
      }
      println();
      for(int i=0; i < 6; i ++){
        val = readArduinoInt();
        println("analog port " + i + "= " + val);
      }
      println("----");
    }
  }
}

int readArduinoInt() 
{
  int val;      
  val = myPort.read();              
  val =  myPort.read() * 256 + val; 
  return val;
}