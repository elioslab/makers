// I2C_Master

#include <Wire.h>

const int address = 4;  

void setup()
{
   Serial.begin(9600);
   Wire.begin();
}

void loop()
{
  if(Serial.available() > 0 )
  {
     // send the data
     char c = Serial.read();
     Wire.beginTransmission(address); // transmit to device
     Wire.write(c);
     Wire.endTransmission();
  }
}
