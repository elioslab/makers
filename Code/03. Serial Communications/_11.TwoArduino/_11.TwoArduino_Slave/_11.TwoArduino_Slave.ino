// I2C_Slave
 
#include <Wire.h>

const int address = 4;  //the address to be used by the communicating devices

void setup()
{
  Serial.begin(9600);
  Wire.begin(address); 
  Wire.onReceive(receiveEvent); 
}

void loop()
{
  // nothing here, all the work is done in receiveEvent
}

void receiveEvent(int howMany)
{
  while(Wire.available() > 0)
  {
    char c = Wire.read(); 
    Serial.println(c); 
  }
}
