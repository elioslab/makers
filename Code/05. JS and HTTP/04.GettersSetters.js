function Container(val){
    var secret = val;

    this.__defineGetter__("secret", function(){
        return secret;
    });

    //this.__defineSetter__("secret", function(val){
    //    secret = val;
    //});
}

var test = new Container(35);

console.log(test.secret);

test.secret = 5;

console.log(test.secret);
