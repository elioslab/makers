// Methods

function speak(line) {
  console.log("The " + this.type + " rabbit says: " + line);
}

var whiteRabbit = { type: "white", speak: speak };
var fatRabbit = { type: "fat", speak: speak };

whiteRabbit.speak("I'm the white");
fatRabbit.speak("I'm the fat");


// Prototypes

var protoRabbit = {
    speak: speak
};

var killerRabbit = Object.create(protoRabbit);
killerRabbit.type = "killer";
killerRabbit.speak("I'm the killer");

// Constructors

function Rabbit(type){
  this.type = type;
  this.teeth = "small";
}

var firstRabbit = new Rabbit("first");
var secondRabbit = new Rabbit("second");

console.log(firstRabbit.type);
console.log(secondRabbit.type);

// Overriding

console.log(firstRabbit.teeth);
firstRabbit.teeth = "long";
console.log(firstRabbit.teeth);
console.log(secondRabbit.teeth);
