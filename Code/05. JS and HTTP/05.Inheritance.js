function Point(x, y) {
  this.x = x;
  this.y = y;
}

Point.prototype.dist = function () {
   return Math.sqrt((this.x*this.x)+(this.y*this.y));
};

Point.prototype.toString = function () {
  return "("+this.x+", "+this.y+")";
};

// Subtyping
function ColorPoint(x, y, color) {
    // It does so by passing this (an instance of ColorPoint)
    // to Point: Point is called as a function, but the call()
    // method allows us to keep the "this" of ColorPoint
    Point.call(this, x, y);
    this.color = color;
}
// We want to inherit Point’s methods
ColorPoint.prototype = Object.create(Point.prototype);

// We specify a new version for toString()
ColorPoint.prototype.toString = function () {
  return this.color+" "+Point.prototype.toString.call(this);
};

var p1 = new Point(1,2);
console.log(p1.toString());

var p2 = new ColorPoint(3,4, "red");
console.log(p2.toString());
