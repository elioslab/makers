 // Load the http server and the model
 var httpServer = require('./servers/http');
 var resources = require('./resources/model');

 // Start the HTTP server by invoking listen() on the Express application
 var server = httpServer.listen(resources.iot.port, function () {
  // Once the server is started the callback is invoked
  console.info('Your WoT API is up and running on port %s', resources.iot.port);
 });
