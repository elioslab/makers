var express = require('express');
var actuatorsRoutes = require('./../routes/actuators');
var sensorRoutes = require('./../routes/sensors');
var resources = require('./../resources/model');
var converter = require('./../middleware/converter');

// Requires the body partes
var bodyParser = require('body-parser');

var app = express();

// Add the bodyParser to the chain
// As the bodyParse middleware get information from
// the request useful for other middleware
// make sure you add it first
app.use(bodyParser.json());

app.use('/iot/actuators', actuatorsRoutes);
app.use('/iot/sensors', sensorRoutes);

app.get('/iot', function (req, res) {
  res.send('This is the WoT API!')
});

!
app.use(converter());

module.exports = app;
