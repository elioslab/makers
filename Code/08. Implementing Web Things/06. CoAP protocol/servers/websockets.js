var WebSocketServer = require('ws').Server;
var resources = require('./../resources/model');

exports.listen = function(server) {
  // Create a WebSockets server by passing it the Express server
  var wss = new WebSocketServer({server: server});
  console.info('WebSocket server started...');
  
  // Triggered after a protocol upgrade when the client connected
  wss.on('connection', function (ws) {
    var url = ws.upgradeReq.url;
    console.info(url);
    try {
      // Register an observer corresponding to the resource in the protocol upgrade URL
      Object.observe(selectResouce(url), function (changes) {
        ws.send(JSON.stringify(selectResouce(url).value), function () {
        });
      })
    }
    // Use a try/catch to catch to intercept errors (e.g., malformed/unsupported URLs)
    catch (e) {
      console.log('Unable to observe %s resource!', url);
    };
  });
};

// This function takes a request URL and returns the corresponding resource
function selectResouce(url) {
  var parts = url.split('/');
  parts.shift();
  var result = resources;
  for (var i = 0; i < parts.length; i++) {
    result = result[parts[i]];
  }
  return result;
}








