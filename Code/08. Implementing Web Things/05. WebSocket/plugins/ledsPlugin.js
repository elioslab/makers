var resources = require('./../resources/model');

var actuator, interval;

var model1 = resources.iot.actuators.leds['1'];
var model2 = resources.iot.actuators.leds['2'];

var pluginName = "Leds"

var localParams = {'simulate': false, 'frequency': 2000};

exports.start = function (params) {
  localParams = params;
  
  // Observe the model for the LEDs
  observe(model1);
  observe(model2);

  if (localParams.simulate) {
    simulate();
  } else {
    connectHardware();
  }
};

exports.stop = function () {
  if (localParams.simulate) {
    clearInterval(interval);
  } else {
    actuator.unexport();
  }
  console.info('%s plugin stopped!', pluginName);
};

function observe(what) {
  // The observe() method is an asynchronous method defined on Object. 
  // It can be used to look for changes of an object and it accepts three parameters:
  // an object to be observed
  // a callback function to be called when a change is detected
  // an optional array containing types of changes to be watched for
  Object.observe(what, function (changes) {
    console.info('Change detected by plugin for %s...', what.name);
    // Listen for model changes, on changes call switchOnOff
    switchOnOff(what.name, what.value);
  });
};

function switchOnOff(name, value) {
  if (!localParams.simulate) {
    var arduino = require('./../hardware/arduino');
    arduino.send(name, value);
    console.info('Changed value of %s to %s', name, value);
  }
};

function connectHardware() {
  console.info('Hardware %s actuator started!', pluginName);
};

function simulate() {
  interval = setInterval(function () {
    // Switch value on a regular basis
    if (model1.value) {
      model1.value = false;
    } else {
      model1.value = true;
    }
  }, localParams.frequency);
  console.info('Simulated %s actuator started!', pluginName);
};
