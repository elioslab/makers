 // Load the http server, the websocekt server and the model
 var httpServer = require('./servers/http');
 var resources = require('./resources/model');
 var wsServer = require('./servers/websockets');

 // Require all the sensor plugins we need
 var ledsPlugin = require('./plugins/ledsPlugin');
 var pirPlugin = require('./plugins/pirPlugin');
 var tempPlugin = require('./plugins/tempPlugin');
 var lightPlugin = require('./plugins/lightPlugin');

 // Start them with a parameter object. Here we start them on a
 // laptop so we activate the simulation function
 ledsPlugin.start({'simulate': false, 'frequency': 1000});
 pirPlugin.start({'simulate': false, 'frequency': 1000});
 tempPlugin.start({'simulate': false, 'frequency': 1000});
 lightPlugin.start({'simulate': false, 'frequency': 1000});


 var server = httpServer.listen(resources.iot.port, function () {
  console.info('Your WoT Pi is up and running on port %s', resources.iot.port);
 });
 
 // Websockets server
 wsServer.listen(server);
 
