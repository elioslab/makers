var serialport = require('serialport');
var values = { temperature:"0", light: "0", movement: "0" }

module.exports = values;

var portName = '/dev/cu.usbmodem1421';

var sp = new serialport.SerialPort(portName, {
    baudRate: 9600,
    dataBits: 8,
    parity: 'none',
    stopBits: 1,
    flowControl: false,
    parser: serialport.parsers.readline("\r\n")
});

sp.on('data', function(input) {
    var res = input.split(";");
    if(res[0] == 'H')
    {
      values.temperature = res[1];
      values.movement = res[2];
      values.light = res[3];
      //console.log(res);
    }
});
