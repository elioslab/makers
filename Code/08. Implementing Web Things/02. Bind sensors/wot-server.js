 // Load the http server and the model
 var httpServer = require('./servers/http');
 var resources = require('./resources/model');

 // Require all the sensor plugins we need
 var ledsPlugin = require('./plugins/ledsPlugin');
 var pirPlugin = require('./plugins/pirPlugin');
 var tempPlugin = require('./plugins/tempPlugin');
 var lightPlugin = require('./plugins/lightPlugin');

 // Start them with a parameter object. Here we start them on a
 // laptop so we activate the simulation function
 ledsPlugin.start({'simulate': true, 'frequency': 2000});
 pirPlugin.start({'simulate': true, 'frequency': 1000});
 tempPlugin.start({'simulate': true, 'frequency': 1000});
 lightPlugin.start({'simulate': true, 'frequency': 1000});

 // Start the HTTP server by invoking listen() on the Express application
 var server = httpServer.listen(resources.iot.port, function () {
  // Once the server is started the callback is invoked
  console.info('Your WoT API is up and running on port %s', resources.iot.port);
 });
