
// Get HTTP and ASSERT to make request
// and test returned values
var http = require('http');
var assert = require('assert');

// An option object containing a list
// of properties
var opts = {
  host: 'localhost',
  port: 8000,
  path: '/send',
  method: 'POST',
  headers: {'content-type':'application/x-www-form-urlencoded'}
}

// HTTP library contains objects to provede clientList
// We create an HTTP request, using the option object
// The callback is attached to the response event for the
// HTTP request
var req = http.request(opts, function(res){
    // define the encoding of all
    // the received data (utf8 == string)
    res.setEncoding('utf8');

    // stream all the responsed from the server
    // in the client we don't have the middleware utility
    var data = "";
    res.on('data', function(d) {
      data += d;
    })

    // when we receive the end event, we can run our test
    // on whatever the server sent
    res.on('end', function() {
      assert.strictEqual(data, '{"status":"ok","message":"Tweet received"}');
    })
  })

// write some data to the server
req.write('tweet=test');
req.end();
