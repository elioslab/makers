
// Loading Express, in order to managing
// HTTP behind the scenes.
var express = require('express');
var bodyParser = require('body-parser');

// Create a server
var app = express();

// Listen is moved on top, this doesn't cause
// race conditions becouse the event loop
app.listen(8000);

var tweets = []

// Instead of attaching listener, we can provide methods
// matching HTTP verbs on specific URL requests
app.get('/',
  function(req, res)
  {
    res.send('Welcome to Node Twitter');
  })

// A POST route for posting tweets, with a middleware (bodyParser()) to stream
// the post data from the client and turn it in a JavaScript object
// It works only with POST encoded in json or urlencoded
app.post('/send', bodyParser.urlencoded(),
  function(req, res)
  {
    // express.bodyParser() adds a property to req called body, it
    // contains an object representing the post data
    // checks if the middleware found any data
    if (req.body && req.body.tweet)
    {
      // store the tweet in an array
      tweets.push(req.body.tweet);

      // send an ok acknowledgement
      // notice that .send() automatically serialize data in a JSON object
      res.send({status:"ok", message:"Tweet received"});
    }
    else
    {
      // send an error acknowledgement
      res.send({status:"nok", message:"No tweet received"});
    }
  })

// a GET route to access all the tweets
app.get('/tweets',
  function(req,res)
  {
    res.send(tweets)
  })
