var serialport = require('serialport');
var plotly = require('plotly')('riccardo.berta','pe8d70wb24');
var token = 'tiiclwz0z5';

var portName = '/dev/cu.usbmodem142321';
var sp = new serialport.SerialPort(portName,{
    baudRate: 9600,
    dataBits: 8,
    parity: 'none',
    stopBits: 1,
    flowControl: false,
    parser: serialport.parsers.readline("\r\n")
});

// helper function to get a nicely formatted date string
function getDateString() {
    var time = new Date().getTime();
    var datestr = new Date(time).toISOString().replace(/T/, ' ').replace(/Z/, '');
    return datestr;
}

var initdata = [{x:[], y:[], stream:{token:token, maxpoints: 500}}];
var initlayout = {fileopt : "extend", filename : "sensor-test"};

plotly.plot(initdata, initlayout, function (err, msg) {
  if (err)
    return console.log(err)
  console.log(msg);
  var stream = plotly.stream(token, function (err, res) {
    console.log(err, res);
});

sp.on('data', function(input) {
    if(isNaN(input) || input > 1023)
      return;
    var streamObject = JSON.stringify({ x : getDateString(), y : input });
    console.log(streamObject);
    stream.write(streamObject+'\n');
  });
});
