var http = require('http');

var request = http.request({
  // telling Node what server to talk to
  hostname: "www.elios.diten.unige.it",
  //what path to request from that server
  path: "/images/resized/new/images/stories/com_form2content/p2/f24/12_228_300.jpg",
  // which method to use
  method: "GET",
  headers: {Accept: "text/html"}
  },
  // the function that should be called when a response comes in
  function(response) {
      console.log("Server responded with status code: ", response.statusCode);
  });

request.end();
