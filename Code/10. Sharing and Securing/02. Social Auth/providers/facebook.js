var passport = require('passport');
var util = require('util');
var FacebookStrategy = require('passport-facebook').Strategy;
var session = require('express-session');
var cookieParser = require('cookie-parser');
var auth = require('../middleware/auth');
var methodOverride = require('method-override');

// Configuration variables: FB app ID, app secret, name, and the URL to call 
// back after a user authentication on Facebook
var acl = require('../config/acl.json');
var facebookAppId = '506972262822180';
var facebookAppSecret = '5be26fcb3a58e3797dacd76529c2a68a';
var socialNetworkName = 'facebook';
var callbackResource = '/auth/facebook/callback';
var callbackUrl = 'https://localhost:' + acl.config.sourcePort + callbackResource;


module.exports.setupFacebookAuth = setupFacebookAuth;

function setupFacebookAuth(app) {
  app.use(cookieParser());
  app.use(methodOverride());
  app.use(session({secret: 'keyboard cat', resave: true, saveUninitialized: true}));
  // Initialize Passport and support storing the user login in sessions
  app.use(passport.initialize());
  app.use(passport.session());

  // If you had a database of users you’d use these two methods to load and save users
  passport.serializeUser(function (user, done) {
    done(null, user);
  });

  passport.deserializeUser(function (obj, done) {
    done(null, obj);
  });

  passport.use(new FacebookStrategy({
        // The credentials used to authenticate your auth proxy as a Facebook app
        clientID: facebookAppId,
        clientSecret: facebookAppSecret,
        // This URL will be called by Facebook after a successful login
        callbackURL: callbackUrl
      },
      function (accessToken, refreshToken, profile, done) {
        // The “verify” function, called by the framework after a successful authentication 
        // with the provider; here we check if the user is known by the proxy and store 
        // their token if so
        auth.checkUser(socialId(profile.id), accessToken, function (err, res) {
          if (err) return done(err, null);
          else return done(null, profile);
        });
      }));

  app.get('/auth/facebook',
    // Trigger the authentication process, and redirect the user to facebook.com
    passport.authenticate('facebook'),
    // Facebook.com will redirect the user to the callbackUrl, 
    // so this function will never be called!
    function (req, res) {});

  // This route will be called by Facebook after user authentication. 
  // If it fails you redirect to /login, otherwise to /account  
  app.get(callbackResource,
    passport.authenticate('facebook', {session: true, failureRedirect: '/login'}),
    function (req, res) {
      res.redirect('/account');
    });

  // If the user is authenticated you get their token and display their account page; 
  // otherwise redirect to /login
  app.get('/account', ensureAuthenticated, function (req, res) {
    auth.getToken(socialId(req.user.id), function (err, user) {
      if (err) res.redirect('/login');
      else {
        req.user.token = user.token;
        res.render('account', {user: req.user});
      }
    });
  });

  // A unique social identifier is formed by concatenating the social userId 
  // and the social network name
  function socialId(userId) {
    return socialNetworkName + ':' + userId;
  };

  app.get('/', ensureAuthenticated, function (req, res) {
    res.render('index', {user: req.user});
  });

  app.get('/login', function (req, res) {
    res.render('login', {user: req.user});
  });

  app.get('/logout', function (req, res) {
    req.logout();
    res.redirect('/');
  });

  function ensureAuthenticated(req, res, next) {
    if (req.isAuthenticated()) {
      return next();
    }
    res.redirect('/login');
  };

};











