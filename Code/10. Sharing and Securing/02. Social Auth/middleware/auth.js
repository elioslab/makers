// Require your ACL config file
var acl = require('../config/acl.json');
var https = require('https');
var fs = require('fs');

exports.socialTokenAuth = function () {
  return function (req, res, next) {
    // If the request is for an open path, call the next middleware
    if (isOpen(req.path)) {
      next();
    } else {
      var token = req.body.token || req.get('authorization') || req.query.token;
      if (!token) {
        return res.status(401).send({success: false, message: 'API token missing.'});
      } else {
        // Otherwise, get the access token and check the ACL for this token
        checkUserAcl(token, req.path, function (err, user) {
          if (err) {
            // If there’s an error, return a 403 Forbidden status code
            return res.status(403).send({success: false, message: err});
          }
          // Otherwise, the user is good to go, and you call the next middleware
          next(); 
        });
      }
    }
  }
};

// Can we find a user with the given token and the given path, for example, /temp?
function checkUserAcl(token, path, callback) {
  var userAcl = findInAcl(function (current) {
    return current.token === token && current.resources.indexOf(path) !== -1;
  });
  if (userAcl) {
    callback(null, userAcl);
  } else {
    callback('Not authorized for this resource!', null);
  }
};
function findInAcl(filter) {
  return acl.protected.filter(filter)[0];
};

// Handle open resources
function isOpen(path) { 
  // Access to any CSS is open...
  if (path.substring(0, 5) === "/css/") return true;

  // Is the path an open path?
  if (acl.open.indexOf(path) !== -1) return true;
}

exports.checkUser = checkUser;
// Called by facebook.js when a user is authenticated
function checkUser(socialUserId, token, callback) {
  var result = findInAcl(function (current) {
    // If the user ID you got from Facebook is present in your ACL, you have a winner!
    return current.uid === socialUserId; 
  });
  if (result) {
    // Store the user token to allow them to make subsequent calls to resources they can access
    result.token = token;
    callback(null, result);
  } else {
    callback('User <b>' + socialUserId + '</b> not found! Did you add it to acl.json?', null);
  }
};

exports.getToken = getToken;
function getToken(socialUserId, callback) {
  var result = findInAcl(function (current) {
    return current.uid === socialUserId;
  });
  if (result) {
    callback(null, result);
  } else {
    callback('User <b>' + socialUserId + '</b> not found! Did you add it to acl.json?', null);
  }
};











