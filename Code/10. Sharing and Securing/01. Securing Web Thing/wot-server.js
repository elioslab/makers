var restApp = require('./servers/http');
var wsServer = require('./servers/websockets');
var resources = require('./resources/model');
var http = require('http');
var fs = require('fs');
  
// We import the https module
var https = require('https');
  
// The private key of the server that we generated before
var key_file = './resources/privateKey.pem';

// The actual certificate of the server
var cert_file = './resources/caCert.pem';

// The password of the private key
var passphrase = 'pippo'; 

var dht22Plugin, pirPlugin, ledsPlugin;

var config = {
  key: fs.readFileSync(key_file),
  cert: fs.readFileSync(cert_file),
  passphrase: passphrase
};

var createServer = function (port, secure, simulate) {
  if (port === undefined) {
    port = resources.customFields.port;
  }

  if (secure === undefined) {
    secure = resources.customFields.secure;
  }

  if(secure) 
  {
    // We create an HTTPS server and pass it the config object  
    return server = https.createServer(config, restApp)
      .listen(process.env.PORT || port, function () {
        console.log('HTTPs server started...');

        // By passing it the server we create, the Websocket library 
        // will automatically detect and enable the TLS support
        wsServer.listen(server);

        initPlugins(port);
      })
  } 
  else 
  {
    return server = http.createServer(restApp)
      .listen(process.env.PORT || port, function () {

        // Websockets server
        wsServer.listen(server); //#F

        console.log('HTTP server started...');
        initPlugins(port);
      })
  }
};

function initPlugins(port) 
{
  var LedsPlugin = require('./plugins/internal/ledsPlugin').LedsPlugin;
  var PirPlugin = require('./plugins/internal/pirPlugin').PirPlugin;
  var Dht22Plugin = require('./plugins/internal/dht22Plugin').Dht22Plugin;

  pirPlugin = new PirPlugin({'simulate': true, 'frequency': 5000});
  pirPlugin.start();

  ledsPlugin = new LedsPlugin({'simulate': true, 'frequency': 5000});
  ledsPlugin.start();

  dht22Plugin = new Dht22Plugin({'simulate': true, 'frequency': 5000});
  dht22Plugin.start();

  console.info('Your WoT server is up and running on port %s', port);
}

module.exports = createServer;

process.on('SIGINT', function () 
{
  ledsPlugin.stop();
  pirPlugin.stop();
  dht22Plugin.stop();
  console.log('Bye, bye!');
  process.exit();
});






