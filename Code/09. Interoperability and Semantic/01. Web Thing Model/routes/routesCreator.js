var express = require('express'),
  router = express.Router(),
  uuid = require('node-uuid'),
  utils = require('./../utils/utils');

exports.create = function (model) {

  createDefaultData(model.links.properties.resources);
  createDefaultData(model.links.actions.resources);

  // Let's create the routes
  createRootRoute(model);
  createModelRoutes(model);
  createPropertiesRoutes(model);
  createActionsRoutes(model);

  return router;
};


function createRootRoute(model) {
  // Handle the Root resource (/)
  router.route('/').get(function (req, res, next) {

    req.model = model;
    req.type = 'root';

    if (model['@context']) type = model['@context'];
    else type = 'http://model.webofthings.io/';

    // Create the Link header containing links 
    // to the other resources (HATEOAS)
    res.links({
      model: '/model/',
      properties: '/properties/',
      actions: '/actions/',
      things: '/things/',
      type: type
    });
    
    // Extract the required fields from the model, add the resulting object to the results
    // We use the functions extractFields() to createe a new object by copying only the
    // necessary fields from the model
    var fields = ['id', 'name', 'description', 'tags', 'customFields'];
    req.result = utils.extractFields(fields, model);
    
    // Call the next middleware (representation middleware)
    next();
  });
};

// The code for the Thing (/), Model (/model) and Properties (/properties/...) and Actions
// (/actions/...) resources is very similar

function createModelRoutes(model) {
  // GET /model
  router.route('/model').get(function (req, res, next) {
    req.result = model;
    req.model = model;

    if (model['@context']) type = model['@context'];
    else type = 'http://model.webofthings.io/';
    res.links({
      type: type
    });

    next();
  });
};

function createPropertiesRoutes(model) {
  var properties = model.links.properties;

  // GET /properties
  router.route(properties.link).get(function (req, res, next) {
    req.model = model;
    req.type = 'properties';
    req.entityId = 'properties';

    req.result = utils.modelToResources(properties.resources, true);

    // Generate the Link headers 
    if (properties['@context']) type = properties['@context'];
    else type = 'http://model.webofthings.io/#properties-resource';

    res.links({
      type: type
    });

    next();
  });

  // GET /properties/{id}
  router.route(properties.link + '/:id').get(function (req, res, next) {
    req.model = model;
    req.propertyModel = properties.resources[req.params.id];
    req.type = 'property';
    req.entityId = req.params.id;

    req.result = reverseResults(properties.resources[req.params.id].data);

    // Generate the Link headers 
    if (properties.resources[req.params.id]['@context']) type = properties.resources[req.params.id]['@context'];
    else type = 'http://model.webofthings.io/#properties-resource';

    res.links({
      type: type
    });

    next();
  });
};


function createActionsRoutes(model) {
  var actions = model.links.actions;

  // GET /actions route
  router.route(actions.link).get(function (req, res, next) {

    req.model = model;
    req.type = 'actions';
    req.entityId = 'actions';

    if (actions['@context']) type = actions['@context'];
    else type = 'http://model.webofthings.io/#actions-resource';

    // Populate the Links Header for the Actions resource
    res.links({
      type: type
    });

    // Transform the model into a Web Thing Model Actions resource
    // we use the helper function modelToResource() to transforms a subset of 
    // the model into an array of resources
    req.result = utils.modelToResources(actions.resources, true);
    
    next();
  });

  // POST /actions/{actionType}
  router.route(actions.link + '/:actionType').post(function (req, res, next) {
    // Extract the request body (Action JSON)
    var action = req.body;
    
    // Create a unique identifier for the new resource and add a timestamp
    action.id = uuid.v1();
    action.status = "pending";
    action.timestamp = utils.isoTimestamp();
    
    // Add the new Action to the model, we add a data field to the model 
    // to contain the actual instance of Actions
    actions.resources[req.params.actionType].data.push(action);
    
    // We add the URL of the new resource to the standard Location header
    res.location(req.originalUrl + '/' + action.id);

    next();
  });

  // GET /actions/{actionType}
  router.route(actions.link + '/:actionType').get(function (req, res, next) {

    req.result = reverseResults(actions.resources[req.params.actionType].data);
    req.actionModel = actions.resources[req.params.actionType];
    req.model = model;

    req.type = 'action';
    req.entityId = req.params.actionType;

    if (actions.resources[req.params.actionType]['@context']) type = actions.resources[req.params.actionType]['@context'];
    else type = 'http://model.webofthings.io/#actions-resource';

    res.links({
      type: type
    });


    next();
  });

  // GET /actions/{id}/{actionId}
  router.route(actions.link + '/:actionType/:actionId').get(function (req, res, next) {
    req.result = utils.findObjectInArray(actions.resources[req.params.actionType].data,
      {"id" : req.params.actionId});
    next();
  });
};

function createDefaultData(resources) {
  Object.keys(resources).forEach(function (resKey) {
    var resource = resources[resKey];
    resource.data = [];
  });
}

function reverseResults(array) {
  return array.slice(0).reverse();
}



