#include <Wire.h>
#include <SPI.h>
#include "SparkFunLSM9DS1.h"

LSM9DS1 imu;

#define LSM9DS1_M	0x1E 
#define LSM9DS1_AG	0x6B
#define DECLINATION 2.12 

void setup() 
{
  Serial.begin(115200);
  
  imu.settings.device.commInterface = IMU_MODE_I2C;
  imu.settings.device.mAddress = LSM9DS1_M;
  imu.settings.device.agAddress = LSM9DS1_AG;
  
  if (!imu.begin())
  {
    Serial.println("Failed to communicate with LSM9DS1.");
    while (1);
  }
}

void loop()
{
  imu.readAccel();
  sendPitchRoll(imu.ax, imu.ay, imu.az);
  delay(25);
}

void sendPitchRoll(float ax, float ay, float az)
{
  float roll = atan2(-ay, az) * 180.0/PI;
  float pitch = atan2(-ax, sqrt(ay * ay + az * az))  * 180.0/PI;
  
  Serial.print(pitch);
  Serial.print(":");
  Serial.println(roll);
}
