// plots 6 analog signals
// 
// The screen can be paused using the spacebar
// pressing the space again resumes running
// 'c' clears the screen
// 'a' starts recording the values in a file
// 'b' stops recording.
// 'p' on/off pitch
// 'r' on/off roll
// everytime 'r' has been pressed a new file will be made

import processing.serial.*;

PrintWriter output;

int pitchgraph = 0;
int rollgraph = 0;

String outputBuff = "";
int linecolor[] = {20,50,100,150,200,240};
int value[] = new int[6];
int filenr;
int diffValue[] = new int[6];
int NEWLINE = 10;
String mode="RUN";
int n;
float scaling = 2;
Serial port;

int w = 800;
int h = 600;

void setup()
{
  size(800,600, P3D);
  port = new Serial(this, Serial.list()[1], 115200);
  port.bufferUntil('\n');
  frameRate(20);
  colorMode(HSB);
  background(255);
}

void draw()
{
  outputBuff="" + millis();
  
  for(int z=0;z<6;z++) 
  {
    stroke(linecolor[z],255,255);
    if(pitchgraph == 1 && (z==0 || z==2 || z == 4))
      line(n-1, diffValue[z] + h/2, n , value[z] + h/2); 
    else if (rollgraph == 1 && (z==1 || z==3 || z == 5))
      line(n-1, diffValue[z] + h/2, n , value[z] + h/2);
    outputBuff += ("," + value[z]);
  }
  
  if (mode=="RECORD") output.println(outputBuff);
 
  if (mode!="PAUSE") n++;
  
  if (n>w)
  { 
    n=0; 
    background(255);
  }
  
  for (int z=0;z<6;z++)
  {
    diffValue[z]=value[z];
  }
  
  /*
  lights();
  pushMatrix();
  translate(width/2, height/2, -30);
  rotateX(((float)value[5])*PI/180.0); 
  rotateZ(((float)value[4])*PI/180.0); 
  box(100);
  popMatrix();
  */
}
 
void serialEvent(Serial port) 
{ 
  String rpstr = port.readStringUntil('\n');
  if (rpstr != null) 
  {
    String[] list = split(rpstr, ':');
    if(list.length == 7)
    {
      value[0] = (int)(float(list[0]) * scaling); 
      value[1] = (int)(float(list[1]) * scaling);
      value[2] = (int)(float(list[2]) * scaling);
      value[3] = (int)(float(list[3]) * scaling);
      value[4] = (int)(float(list[4]) * scaling);
      value[5] = (int)(float(list[5]) * scaling);
      
      println("pitchAcc : "  + value[0] + "\t" +
              "rollAcc : "   + value[1] + "\t" +
              "pitchGyro : " + value[2] + "\t" +
              "rollGyro : "  + value[3] + "\t" +
              "pitch : "     + value[4] + "\t" +
              "roll : "      + value[5] );
    }
  }
}

void keyPressed()
{
  if (key==' ' && mode=="RUN")
  {
    mode="PAUSE";
  }
  else if (key==' ' && mode=="PAUSE") 
  {
    mode="RUN";
  }
  if (key=='a' && mode!="RECORD")  
  {
    mode="RECORD";
    filenr++;
    output = createWriter("values"+filenr+".txt");
  }
  if (key=='b') 
  {
    mode="STOP";
    output.flush();
    output.close();
  }
  if (key=='c') 
  {
    n=0; 
    background(255);
  }
  if (key=='w') 
  {
    scaling += 0.5;
  }
  if (key=='z') 
  {
    scaling -= 0.5;
  }
  if (key=='p')
  {
    pitchgraph = 1 - pitchgraph;
  }
  if (key=='r')
  {
    rollgraph = 1 - rollgraph;
  }
}