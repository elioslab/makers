// Measuring Distance
// Connection
//
// SRF02       Arduino
// 5v Vcc    -> 5V
// SDA       -> A4
// SCL       -> A5
// Mode      -> no connection (I2C or UART)
// 0v Ground -> GND


#include <Wire.h>

void setup()
{
  // join i2c bus (address optional for master)
  Wire.begin();                
  Serial.begin(9600);          
}

int reading = 0;

void loop()
{
  // step 1: instruct sensor to read echoes

  // transmit to device #112 (0x70)
  Wire.beginTransmission(112); 
  
  // sets register pointer to the command register (0x00)
  Wire.write(byte(0x00));
  
  // command sensor to measure in "centimeters"
  // use 0x51 for centimeters
  // use 0x52 for ping microseconds
  Wire.write(byte(0x51));      
  
  Wire.endTransmission();     


  // step 2: wait for readings to happen
  // datasheet suggests at least 65 milliseconds
  delay(70);                   


  // step 3: instruct sensor to return a particular echo reading
  
  Wire.beginTransmission(112); 
  
  // sets register pointer to echo #1 register (0x02)
  Wire.write(byte(0x02));      
  
  Wire.endTransmission();      

  
  // step 4: request reading from sensor

  // request 2 bytes from slave device #112
  Wire.requestFrom(112, 2);    


  // step 5: receive reading from sensor
  if (2 <= Wire.available())   // if two bytes were received
  {
    reading = Wire.read();    // receive high byte (overwrites previous reading)
    reading = reading << 8;   // shift high byte to be high 8 bits
    reading |= Wire.read();   // receive low byte as lower 8 bits
    
    Serial.print(reading);    
    Serial.println("cm");
  }

  delay(250);                  
}
