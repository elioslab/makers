// Detecting Motion

int pirPin = 2;

void setup()
{
  Serial.begin(9600); 
  pinMode(pirPin, INPUT);
}


void loop()
{
  int pirVal = digitalRead(pirPin);

  // motion detected
  if(pirVal == LOW)
  { 
    Serial.println("Motion Detected"); 

    // The sensor needs 2 seconds to take an 
    // image to compare to
    delay(2000); 
  }
}
