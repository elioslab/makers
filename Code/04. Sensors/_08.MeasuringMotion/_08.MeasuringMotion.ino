// The LSM9DS1 is a versatile 9DOF sensor. It has a built-in
// accelerometer, gyroscope, and magnetometer. 
// It functions over either I2C (and SPI).

// Demo the following:
// How to create a LSM9DS1 object
// How to use the begin() function of the LSM9DS1 class.
// How to read the gyroscope, accelerometer, and magnetometer
// How to calculate actual acceleration, rotation speed, and magnetic field strength

// The LSM9DS1 has a maximum voltage of 3.6V. Make sure you power it
// off the 3.3V rail

#include <Wire.h>
#include <SPI.h>
#include "SparkFunLSM9DS1.h"


// LSM9DS1 Library Init
LSM9DS1 imu;

//I2C addresses:
#define LSM9DS1_M	0x1E 
#define LSM9DS1_AG	0x6B

#define PRINT_SPEED 250 

// Earth's magnetic field varies by location. Add or subtract 
// a declination to get a more accurate heading. Calculate 
// your's here:
// http://www.ngdc.noaa.gov/geomag-web/#declination
#define DECLINATION 2.12 

void setup() 
{
  Serial.begin(115200);
  
  // Before initializing the IMU, there are a few settings we may need to adjust. 
  // Use the settings struct to set the device's communication mode and addresses:
  imu.settings.device.commInterface = IMU_MODE_I2C;
  imu.settings.device.mAddress = LSM9DS1_M;
  imu.settings.device.agAddress = LSM9DS1_AG;
  
  // The above lines will only take effect AFTER calling imu.begin(), which verifies 
  // communication with the IMU and turns it on.
  if (!imu.begin())
  {
    Serial.println("Failed to communicate with LSM9DS1.");
    while (1);
  }
}

void loop()
{
  imu.readAccel();
  imu.readGyro();
  imu.readMag();

  printValues();
 
  delay(PRINT_SPEED);
}

void printValues()
{
  Serial.print(imu.calcAccel(imu.ax), 2);
  Serial.print(":");
  Serial.print(imu.calcAccel(imu.ay), 2);
  Serial.print(":");
  Serial.print(imu.calcAccel(imu.az), 2);
  Serial.print(":");
  Serial.print(imu.calcGyro(imu.gx), 2);
  Serial.print(":");
  Serial.print(imu.calcGyro(imu.gy), 2);
  Serial.print(":");
  Serial.print(imu.calcGyro(imu.gz), 2);
  Serial.print(":");
  Serial.print(imu.calcMag(imu.mx), 2);
  Serial.print(":");
  Serial.print(imu.calcMag(imu.my), 2);
  Serial.print(":");
  Serial.print(imu.calcMag(imu.mz), 2);
  Serial.println(":");
}

